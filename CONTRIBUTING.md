# Contribuir

Este sitio está creado con hugo, por lo que es necesario tener unas nociones básicas de markdown, html y javascript. Si no tienes este conocimiento y quieres contribuir, por favor crear un mail a ever.developer3001@gmail.com con el asunto "[GRIMORIO-FR]" explicando los errores o características a implementar.

## Código

Si se requiere arreglar un error o implementar una característica, por favor hacer un fork del repositorio y crear un Pull Request.

Antes de empezar cualquier Pull Request, se recomienda crear un issue para discutir primero. De esta manera puede se puede confirmar con los contribuidores si están de acuerdo, además de dar las instrucciones de cómo realizar el proceso. Esto permite tener un proceso más rápido.

Los Pull Requests sólo pueden ser fusionados una vez que todas las comprobaciones de estado estén en verde.

## No realizar push force a la branch del Pull Request

Por favor, no hacer un push force a la branch en la que se hace el Pull Request, ya que al hacerlo se dificulta la revisión de los cambios.
